FROM php:7.4-alpine as installer

ARG DEPLOYERVERSION=*

COPY --from=composer /usr/bin/composer /usr/bin/composer

RUN mkdir -p /tmp/deployer && \
    cd /tmp/deployer && \
	/usr/bin/composer require deployer/deployer:${DEPLOYERVERSION}

FROM php:7.4-alpine

RUN mkdir -p /tools
COPY --from=installer /tmp/deployer /tools/deployer
RUN ln -s  /tools/deployer/vendor/bin/dep /usr/bin/dep

RUN apk add --update --no-cache openssh rsync